# TND4 System Readme
Note: This is a first draft for a Readme. A lot of information is still missing and will be added.

## Goals

TND4 is a currently in development alternative to the SMC3 family of programs for DIY motion simulators.

The TND4 System includes:
- TND4: The arduino program which communicates with the computer, and drives the motors accordingly.
- TND4cli: A command-line app to quickly change settings of a TND4 device, or quickly configure a TND4 device with a premade configuration file.
- TND4core: A graphical software to visualise the inputs and outputs of a TND4 device and adjust settings.

A list of goals are:
* TND4 to be a drop in replacement for SMC3
* Improve TND4 with additional functionality


Potential Features of TND4core:
* Tab View mode, with capability to detach tabs into a seperate window to work on 2 motors at once.

Potential Features of TND4cli:
* Shell-like mode.

Potential Features of the TND4 System:
* Ability to set passcode to lock the TND4 device.

## Roadmap
### Q1 to Q2 of 2023
* Basic compatibility of TND4 with software SMC3 Utils. First Beta release.
### Q3 of 2023
* Full compatibility with Simtools patched games and first official full release.
### Q3 to Q4 of 2023
* First official release of TND4cli with basic features.
### Q4 of 2023 to Q1 2024
* First release of TND4core with basic features.
### 2024
* Continuous improvements and bugfixes to all 3 parts of the TND4 System.

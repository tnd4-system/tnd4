#include "SMC3functions.h"
//Block of stuff taken from SMC3. Its gonna stay here until I find a better solution.
// https://github.com/SimulatorMotorController/SMC3

// defines for setting and clearing register bits




void smcPWMWrite(uint8_t pin, uint8_t val)
{
    #define OCR1A_MEM      0x88
    #define OCR1B_MEM      0x8A

    pinMode(pin, OUTPUT);
	
    //casting "val" to be larger so that the final value (which is the partially
    //the result of multiplying two potentially high value int16s) will not truncate
    uint32_t tmp = val;
	
    if (val == 0)
        digitalWrite(pin, LOW);
    else if (val == 255)
        digitalWrite(pin, HIGH);
    else
    {
        uint16_t regLoc16 = 0;
        uint16_t top;
		
        switch(pin)
        {
            case 9:
                sbi(TCCR1A, COM1A1);
                regLoc16 = OCR1A_MEM;
                top = ICR1;                  //Timer1_GetTop();
                break;
            case 10:
                sbi(TCCR1A, COM1B1);
                regLoc16 = OCR1B_MEM;
                top = ICR1;                  //Timer1_GetTop();
                break;
        }
        tmp=(tmp*top)/255;
        _SFR_MEM16(regLoc16) = tmp;       //(tmp*top)/255;
    }		
} 


void setPwmFrequency(int pin, int divisor){ //Credit: SMC3: Setup the PWM pin frequencies

    byte mode;
    if(pin == 5 || pin == 6 || pin == 9 || pin == 10) 
    {
        switch(divisor) 
        {
            case 1: mode = 0x01; break;
            case 8: mode = 0x02; break;
            case 64: mode = 0x03; break;
            case 256: mode = 0x04; break;
            case 1024: mode = 0x05; break;
            default: return;
        }
        if(pin == 5 || pin == 6) 
        {
            TCCR0B = TCCR0B & 0b11111000 | mode;
        } 
        else 
        {
            TCCR1B = TCCR1B & 0b11111000 | mode;
        }
    } 
    else 
    {
        if(pin == 3 || pin == 11) 
        {
            switch(divisor) 
            {
                case 1: mode = 0x01; break;
                case 8: mode = 0x02; break;
                case 32: mode = 0x03; break;
                case 64: mode = 0x04; break;
                case 128: mode = 0x05; break;
                case 256: mode = 0x06; break;
                case 1024: mode = 0x7; break;
                default: return;
            }
            TCCR2B = TCCR2B & 0b11111000 | mode;
        }
    }
}

void  InitialisePWMTimer1(unsigned int Freq){     // Used for pins 9 and 10

	uint8_t wgm = 8;    //setting the waveform generation mode to 8 - PWM Phase and Freq Correct
	TCCR1A = (TCCR1A & B11111100) | (wgm & B00000011);
	TCCR1B = (TCCR1B & B11100111) | ((wgm & B00001100) << 1);
	TCCR1B = (TCCR1B & B11111000) | 0x01;    // Set the prescaler to minimum (ie divide by 1)

	unsigned int CountTOP;

        CountTOP = (F_CPU / 2) / Freq;    // F_CPU is the oscillator frequency - Freq is the wanted PWM freq

        // Examples of CountTOP:
	//  400 = 20000Hz  -> 8MHz / Freq = CountTOP
        //  320 = 25000Hz
        //  266 = 30075Hz 

	ICR1 = CountTOP;          // Set the TOP of the count for the PWM
}

void InitialisePWMTimer2(unsigned int Freq){
    int Timer2DivideMode;
    
    if (Freq>=15000)   
    {
       Timer2DivideMode=0x01;   // Anything above 15000 set divide by 1 which gives 31250Hz PWM freq
    }
    else
    {
       Timer2DivideMode=0x02;   // Anything below 15000 set divide by 8 (mode 2) which gives 3906Hz PWM freq
    }
    TCCR2B = TCCR2B & 0b11111000 | Timer2DivideMode;
}

//END of SMC3 code Block.

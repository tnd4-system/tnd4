#ifndef EDITABLES_H
#define EDITABLES_H

//User Defined Settings.
//Change the pins to your liking here.
//Each pin should be unique. Do not duplicate pins.
#define MOTOR1_L_EN 2
#define MOTOR1_R_EN 3
#define MOTOR1_PWM 9
#define MOTOR1_POS A0

#define MOTOR2_L_EN 4
#define MOTOR2_R_EN 5
#define MOTOR2_PWM 10
#define MOTOR2_POS A1

#define MOTOR3_L_EN 6
#define MOTOR3_R_EN 7
#define MOTOR3_PWM 11
#define MOTOR3_POS A2




#endif // END EDITABLES
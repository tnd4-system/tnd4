#include "globals.h"
#include "userEditables.h"
#include "motor.h"


motor motor1(MOTOR1_L_EN, MOTOR1_R_EN, MOTOR1_PWM, MOTOR1_POS, 1);
motor motor2(MOTOR2_L_EN, MOTOR2_R_EN, MOTOR2_PWM, MOTOR2_POS, 2);
motor motor3(MOTOR3_L_EN, MOTOR3_R_EN, MOTOR3_PWM, MOTOR3_POS, 3);

statusCodeBase statusCode = WORKING_NORMALLY;

uint8_t RxBuffer[11];  //Byte Buffer for received messages
uint8_t RxByte{};       //Newest received byte
int8_t RxState{};       //
long errorcount = 0;  //Serial Connection Error Counter

unsigned int Timer1FreqkHz = 25;   // PWM freq used for Motor 1 and 2
unsigned int Timer2FreqkHz = 31;   // PWM freq used for Motor 3
#ifndef GLOBALS_H
#define GLOBALS_H

#include "motor.h"
#include <Arduino.h>
#include <stdint.h>
#include "userEditables.h"

//Globals

//#define verboseOutput

#define SMC3_START_BYTE '['  // Start Byte for SMC3 style serial commands
#define SMC3_END_BYTE ']'    // End Byte for SMC3 style  serial commands
//SMC3 type commands [XXX]

#define START_BYTE '<'       // Start Byte for potential new TND4 command set
#define END_BYTE '>'         // End Byte for TND4 command set
//TND4 commands <XXXXXXXX> Variable length. Must start with < and end with >. 8 chars inbetween max.

#define VERSION "v0.0 Prerelease version"

extern uint8_t RxBuffer[11];  //Byte Buffer for received messages
extern uint8_t RxByte;       //Newest received byte
extern int8_t RxState;       //
extern long errorcount;  //Serial Connection Error Counter

extern unsigned int Timer1FreqkHz;   // PWM freq used for Motor 1 and 2
extern unsigned int Timer2FreqkHz;   // PWM freq used for Motor 3

enum statusCodeBase{
WORKING_NORMALLY = 0,
OUT_OF_BOUNDS  =  4,
PINS_MISCONFIGURED = 7,
};

extern motor motor1;
extern motor motor2;
extern motor motor3;

extern statusCodeBase statusCode;

#endif // END GLOBALS

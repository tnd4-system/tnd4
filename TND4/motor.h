
#ifndef MOTOR_H
#define MOTOR_H
#include <Arduino.h>
#include <stdint.h>

//Motor class file for clarity and troubleshooting. shoutout to GPT4

//Motor controll base class
class motor {

private:
  short int currentPos;  //Potentiometer Wiper/Hall Sensor Feedback Value
  short int goalPos;     //Value of where the motor should move to
  bool isActive;         //Save Activity Status

  //h bridge control pins
  uint8_t PWMpin;
  uint8_t L_ENpin;
  uint8_t R_ENpin;

  uint8_t POSpin;  //Connection pin for Hall-Effect or Potentiometer sensor.

  //internal motor values
  uint8_t deadzone;
  short int maxPosition = 10000; //value only for testing  //Maximum Position the motor should move to. Clip Input Max
  short int minPosition = 10000; //value only for testing //Minimum Position the motor should move to. Clip Input Min
  short int maxBoundary;  //Maximum boundary, if motor exits the boundary the device gets locked.
  short int minBoundary;  //Minimun boundary, if motor exits the boundary the device gets locked.
  short int pwmMax;
  short int pwmRev;  //Being Phased out
  short int positionOffset;
  bool autoActivate;
  short int Ks;
  short int Kp_x100;
  short int Ki_x100;
  short int Kd_x100;
  short int target;

  void updateCurPosition(void) {
    currentPos = analogRead(POSpin);
  }


public:
  uint8_t motorID;
  uint8_t pinCheck[4]; //New Array for new Duplicate pin checks at runtime.

  motor(const uint8_t &L_ENpinNew, const uint8_t &R_ENpinNew, const uint8_t &PWMpinNew, const uint8_t &POSpinNew, const uint8_t &IDnew) {  //motor class constructor

    this->pinCheck[0] = L_ENpinNew;
    this->pinCheck[1] = R_ENpinNew;    
    this->pinCheck[2] = PWMpinNew;
    this->pinCheck[3] = POSpinNew;
    this->motorID = IDnew;
    
    this->PWMpin = PWMpinNew;
    this->L_ENpin = L_ENpinNew;
    this->R_ENpin = R_ENpinNew;
    this->POSpin = POSpinNew;
  }

  void init(void);

  void activate(void) {
    this->isActive = true;
  }

  void deactivate(void) {
    digitalWrite(this->L_ENpin, LOW);
    digitalWrite(this->R_ENpin, LOW);
    analogWrite(this->PWMpin, 0);

    this->isActive = false;
  }

  bool inBoundaries(void) {
    #ifdef verboseOutput
    Serial.println("Called inBoundaries")
    #endif    
    this->updateCurPosition();
    return ((currentPos > minBoundary && currentPos < maxBoundary) || !isActive);
  }

  bool inLimits(void) {
    this->updateCurPosition();
    return ((currentPos > minPosition && currentPos < maxPosition) || !isActive);
  }

  void newTarget( uint8_t buffer1, uint8_t buffer2){
    int newTarget = buffer1*256 + buffer2;
    if (newTarget > this->maxPosition) newTarget = this->maxPosition;
    else if (newTarget < this->minPosition) newTarget = this-> minPosition;
    this->target = newTarget;
    Serial.println(newTarget);

    #ifdef verboseOutput
      Serial.print("Called newTarget with ");
      Serial.print(buffer1);
      Serial.print(" and ");
      Serial.print(buffer2);
      Serial.print("Returning with ");
      Serial.print(targetValue);   
      Serial.print("\n");      
    #endif    
  }

   void newKp( uint8_t buffer1, uint8_t buffer2){
    int newKp = buffer1*256 + buffer2;
    this->Kp_x100 = newKp;
   }    

};  //End Motor Class

#endif // MOTOR_H


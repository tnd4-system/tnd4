//If you change anything beyond this point, the program may break. 
//Please only do so if you know what you are doing.
#include "protothreads.h"
#include <EEPROM.h>
#include <avr/wdt.h>

#include "motor.h"
#include "userEditables.h"
#include "globals.h"
#include "SMC3functions.h"

//(L_ENpinNew, R_ENpinNew, PWMpinNew,  POSpinNew)


bool checkDuplicates(const motor &candidateA, const motor &candidateB){ //Return True if pins are duplicated

  #ifdef verboseOutput
  Serial.print("Called checkDuplicates\n");
  Serial.print("Checking for motors: ");
  Serial.print(candidateA.motorID);
  Serial.print(" and ");
  Serial.print(candidateB.motorID);
  Serial.print("\n");
  #endif

  for (int i = 0; i < 4; i++) { //Cycle through candidate A  
      for (int x = 0; x < 4; x++) { //cycle through candidate B
        if ( (candidateA.motorID == candidateB.motorID) && (i == x) ){} //don't check if the same pin is being checked.
          else { 
                #ifdef verboseOutput 
                Serial.print("Check for pins: ");                
                Serial.print(i);
                Serial.print(" and ");
                Serial.print(x);
                Serial.print("\n");
                #endif

                if (candidateA.pinCheck[i] == candidateB.pinCheck[x])  return true; }
        }
      }
  
  return false;
}



float analogToVoltage(const short int &analog) {
  return (analog * 0.488758553);  //take the alalog value 0...1023 and generate a float volage e.g. 1.3
}



class EEPROMmanager {

private:

public:

  void saveParams(void) {

    //saving member function not started.
  }

  short int readVoltage(const short int &address) {  //Voltages are stored in 2 eeprom addresses. one for the characteristic, one address higher for the mantissa //done
    byte tempVol1 = EEPROM.read(address);
    byte tempVol2 = EEPROM.read(address + 1);
    byte tempVol3 = EEPROM.read(address + 2);         //IF this is 100 the voltage is negative
    float tempVoltage = tempVol1 + (tempVol2 / 100);  //create float from 2 byte values
    if (tempVol3 == 100) {
      tempVoltage *= -1;
    }
    return voltageToAnalog(tempVoltage);
  }

  void saveAnalogVoltage(const short int &address, const short int &analogVoltage, const bool &negative) { //done
    float tempVol = analogVoltage * 0.004887586;
    uint8_t tempCharacteristic = tempVol;
    float tempMantissa = (tempVol - tempCharacteristic) * 100;  //Get rid of the characteristic, make the mantissa characteristic 2.83 -> 83
    EEPROM.update(address, tempCharacteristic);
    EEPROM.update(address + 1, tempMantissa);

    if (negative == true) {
      EEPROM.update(address + 2, 100); //characteristic address + 2 shows if a number should be negative or not.100 means negative
    } else {
      EEPROM.update(address + 2, 0);
    }
  }


  void savePercentage(const short int &address, uint8_t &percentage) { //done
    if (percentage > 100) {
      percentage = 100;
    }

    EEPROM.update(address, percentage);
  }

  uint8_t readPercentage(const short int &address) { //done
    return EEPROM.read(address);
  }



};  //End Class EEPROMmanager



short int voltageToAnalog(const float &voltage) {

  short int tempVoltage = voltage * 100;  //Takes voltage value example 3.30V makes it 330. because map() does not work with floats

  return map(tempVoltage, 0, 500, 0, 1023);
}



void reboot(void) {  //Restarts the Arduino Device after being called.
  wdt_disable();
  wdt_enable(WDTO_15MS);
  while (1) {}
}



//Send ID and values to PC software after send request
//SMC3 Compatible
void sendComs(const short int &id, const short int &value) {
  short int low, high;

  high = value / 256;
  low = value - (high * 256);

  Serial.write(SMC3_START_BYTE);
  Serial.write(id);
  Serial.write(high);
  Serial.write(low);
  Serial.write(SMC3_END_BYTE);
}

//Send ID and Values to PC software after send request
//SMC3 compatible
void send2Coms(const short int &id, const short int &v1, const short int &v2) {
  short int low, high;

  Serial.write(SMC3_START_BYTE);
  Serial.write(id);
  Serial.write(v1);
  Serial.write(v2);
  Serial.write(SMC3_END_BYTE);
}





//Emergency stop function. 
void emergencyStop(void) {
  Serial.print("Emergency Stop!\n");
  motor1.deactivate();
  motor2.deactivate();
  motor3.deactivate();
  Serial.print("Stopped Motors.\n");
}

void checkAllDuplicates(void){
  Serial.print("Checking for duplicate pins: ");
  if((checkDuplicates(motor1, motor1) == true ) ||
  (checkDuplicates(motor1, motor2) == true) ||
  (checkDuplicates(motor1, motor3) == true) ||
  (checkDuplicates(motor2, motor2) == true) ||
  (checkDuplicates(motor2, motor3) == true) ||
  (checkDuplicates(motor3, motor3) == true) ){
    
    emergencyStop();
    statusCode = PINS_MISCONFIGURED;
    return; 
    } 

    Serial.print("None Found.\n");
}


EEPROMmanager storage;

//Thread for checking if all motors are in boundaries.
pt ptBoundaryControl;
int boundaryControlThread(struct pt *pt) {
  PT_BEGIN(pt);

  if (motor1.inBoundaries() == true && motor2.inBoundaries() == true && motor3.inBoundaries() == true) {}
  else {
    emergencyStop();
    statusCode = OUT_OF_BOUNDS;
  }

  PT_SLEEP(pt, 50);
  PT_END(pt);
}


void motor::init(void) {
  Serial.print("Start motor::init\n");

    pinMode(this->PWMpin, OUTPUT);
    pinMode(this->L_ENpin, OUTPUT);
    pinMode(this->R_ENpin, OUTPUT);
    pinMode(this->POSpin, INPUT);
    return;

  Serial.print("End Motor Init\n");
}


pt ptErrorLED;
int errorLEDthread(struct pt *pt) {
  PT_BEGIN(pt);
  //NOTE: potentially refactor this code again to use a function for short and long blink.
  //I tried once so far and I did not get it working with the thread.
  while (statusCode == WORKING_NORMALLY) {           //Working Normally
    digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
    PT_SLEEP(pt, 100);
    digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
    PT_SLEEP(pt, 100);
    digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
    PT_SLEEP(pt, 100);
    digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
    PT_SLEEP(pt, 100);
    PT_SLEEP(pt, 1000);
  }

  while (statusCode == OUT_OF_BOUNDS) {             //Out of boundaries
    while (1) {                         //this part does not use PT_SLEEP to lock the device.
      digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);  // turn the LED off by making the voltage LOW
      delay(100);
      delay(1500);
    }
  }

    while(statusCode == PINS_MISCONFIGURED){ //Device Locked, Misconfigured Motor Pins
    Serial.print("Error: Misconfigured Motor Pins. Device Locked!\n");
      while(1){ //This part does not use PT_SLEEP. Therefore once called it will take all CPU time until the machine gets reset.
      
      digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
      delay(500);

      digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
      delay(500);

      digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
      delay(1500);
      
      }
    }

  PT_END(pt);
}




pt ptComms;
int commsThread(struct pt *pt) {  //Serial communications thread.
  PT_BEGIN(pt);
  //Serial.print("Start Comms thread\n");

    for (uint8_t i = 0; i < 6; i++) { //Reset buffer
    RxBuffer[i] = 0;
  }

  if (Serial.available() > 0) { //if Serial is available read it up to ] or 6 chars.
    Serial.readBytesUntil(SMC3_END_BYTE, RxBuffer,11);
      #ifdef verboseOutput
      Serial.print("Serial became available\n");
      #endif
    if (RxBuffer[0] == SMC3_START_BYTE) parseCommand();
    else if (RxBuffer[0] == START_BYTE) parseCommandTND4();
    else { 
      Serial.print("Communications Error: Not parsed\n");
      errorcount++;
      }
  }

  PT_SLEEP(pt, 2);
  PT_END(pt);
}


void parseCommandTND4(){
      #ifdef verboseOutput
      Serial.print("called parseCommandTND\n");
      #endif
  //guard clause for missing < or too long
  if (RxBuffer[0] != START_BYTE || RxBuffer[10] != 0) {
    errorcount++;
    Serial.print("Parse Error: No start byte or message in wrong format\n");    
    return;
  }
  // <reset> to reboot the TND4 device.
  if ( (RxBuffer[1] == 'r') && (RxBuffer[2] == 'e') && (RxBuffer[3] == 's') && (RxBuffer[4] == 'e') && (RxBuffer[5] == 't') ) {
    reboot();
    return;
  }

  if ( (RxBuffer[1] == 't') && (RxBuffer[2] == 'i') && (RxBuffer[3] == 'm') && (RxBuffer[4] == 'e') ) {
  Serial.println( millis() );
  return;
  }

  
  Serial.println("No command.");
} //End parseCommandTND






void parseCommand(void) {
      #ifdef verboseOutput
      Serial.print("called parseCommand\n");
      #endif

  //guard clause: stop parse if start byte isnt given or SMC3 command is too long
  if (RxBuffer[0] != SMC3_START_BYTE || RxBuffer[4] != 0) {
    errorcount++;
    Serial.print("Parse Error: No start byte or message in wrong format\n");    
    return;
  }


  //[Dxx] Send the Kp for motor 1
  if ( RxBuffer[1] == 'D' ) {
  motor1.newKp(RxBuffer[2],RxBuffer[3]);
  return;
  }

  //[Exx] Send the Kp for motor 2
  if ( RxBuffer[1] == 'E' ) {
  motor2.newKp(RxBuffer[2],RxBuffer[3]);
  return;
  }

   //[Fxx] Send the Kp for motor 3
  if ( RxBuffer[1] == 'F' ) {
  motor3.newKp(RxBuffer[2],RxBuffer[3]);
  return;
  }

  //[AXX] A set new motor1 position XX is binary position value
  if (RxBuffer[1] == 'A') {
    motor1.newTarget(RxBuffer[2],RxBuffer[3]);
  }

  //[BXX] B set new motor2 position XX is binary position value
  if (RxBuffer[1] == 'B') {
    motor2.newTarget(RxBuffer[2],RxBuffer[3]);
  }

    //[AXX] C set new motor3 position XX is binary position value
  if (RxBuffer[1] == 'C') {
    motor3.newTarget(RxBuffer[2],RxBuffer[3]);
  }

//[rdx] x indicated which value. rd means send these values from device.
  if ( (RxBuffer[1] == 'r') &&  (RxBuffer[2] == 'd') ) {
                  

  }

  //[ver] print version number
  if ( (RxBuffer[1] == 'v') && (RxBuffer[2] == 'e') && (RxBuffer[3] == 'r') ) {
    Serial.println(VERSION);
    return;
  }


  //enable all motors / SMC3 Compatible
  if ( (RxBuffer[1] == 'e') && (RxBuffer[2] == 'n') && (RxBuffer[3] == 'a') ) {
    Serial.print("Start motor Received\n");
    motor1.activate();
    motor2.activate();
    motor3.activate();
    return;
  }

  //Save all parameters to storage [sav]
  if ( (RxBuffer[1] == 's') && (RxBuffer[2] == 'a') && (RxBuffer[3] == 'v') ){
    Serial.print("Save params received\n");
    //storage.saveParams();
  }







}  //End Parse Command Function




void setup(void) {
  Serial.begin(500000);
  Serial.print("\nWelcome to TND4!\nStarting TND4 Device\n");

  #ifdef verboseOutput
  Serial.print("WARNING: VERBOSE OUTPUT ACTIVE!\nPROGRAM WILL RUN SLOWER THAN NORMAL!\n");
  Serial.print("Remove #ifdef verboseOutput at start of the TND4.ino file and reupload sketch\n");
  delay(1000);
  #endif

  Serial.print("Initialising Threads\n");
  PT_INIT(&ptErrorLED);
  PT_INIT(&ptBoundaryControl);
  PT_INIT(&ptComms);

  Serial.print("Initialising IO and motor objects\n");
  pinMode(LED_BUILTIN, OUTPUT);


  //InitialisePWMTimer1(Timer1FreqkHz * 1000);        // PWM Freq for Motor 1 and 2 can be set quite precisely - pass the desired freq and nearest selected
 // InitialisePWMTimer2(Timer2FreqkHz * 1000);        // Motor 3 PWM Divider can only be 1, 8, 64, 256 giving Freqs 31250,3906, 488 and 122 Hz
    
  // set analogue prescale to 16
  sbi(ADCSRA,ADPS2);
  cbi(ADCSRA,ADPS1);
  cbi(ADCSRA,ADPS0);

  motor1.init();
  motor2.init();
  motor3.init();
  checkAllDuplicates();
  
  Serial.print("Finished setup routine in: ");
  Serial.print(millis());
  Serial.print("ms\n");
}



static_assert( ((MOTOR1_PWM == 3) || (MOTOR1_PWM == 5) || (MOTOR1_PWM == 6) || (MOTOR1_PWM ==9) || (MOTOR1_PWM == 10) || (MOTOR1_PWM == 11) ), "TND4-Warning: Misconfigured pins: Selected PWM pin for motor 1 is not PWM capable! Please fix in lines 9...22" ); 
static_assert( ((MOTOR2_PWM == 3) || (MOTOR2_PWM == 5) || (MOTOR2_PWM == 6) || (MOTOR2_PWM ==9) || (MOTOR2_PWM == 10) || (MOTOR2_PWM == 11) ), "TND4-Warning: Misconfigured pins: Selected PWM pin for motor 2 is not PWM capable! Please fix in lines 9...22" ); 
static_assert( ((MOTOR3_PWM == 3) || (MOTOR3_PWM == 5) || (MOTOR3_PWM == 6) || (MOTOR3_PWM ==9) || (MOTOR3_PWM == 10) || (MOTOR3_PWM == 11) ), "TND4-Warning: Misconfigured pins: Selected PWM pin for motor 3 is not PWM capable! Please fix in lines 9...22" ); 
static_assert( ((MOTOR1_POS == A0) || (MOTOR1_POS == A1) || (MOTOR1_POS == A2) || (MOTOR1_POS == A3) || (MOTOR1_POS == A4) || (MOTOR1_POS == A5) ), "TND4-Warning: Misconfigured pins: Selected POS pin for motor 1 is not an analog pin! Please fix in lines 9...22" );
static_assert( ((MOTOR2_POS == A0) || (MOTOR2_POS == A1) || (MOTOR2_POS == A2) || (MOTOR2_POS == A3) || (MOTOR2_POS == A4) || (MOTOR2_POS == A5) ), "TND4-Warning: Misconfigured pins: Selected POS pin for motor 2 is not an analog pin! Please fix in lines 9...22" ); 
static_assert( ((MOTOR3_POS == A0) || (MOTOR3_POS == A1) || (MOTOR3_POS == A2) || (MOTOR3_POS == A3) || (MOTOR3_POS == A4) || (MOTOR3_POS == A5) ), "TND4-Warning: Misconfigured pins: Selected POS pin for motor 3 is not an analog pin! Please fix in lines 9...22" );


#pragma message ("If you encounter a problem with this program, report an issue on https://gitlab.com/tnd4-system/tnd4/-/issues. Always state which version you were using.")
#pragma message VERSION


void loop(void) {
  

  PT_SCHEDULE(errorLEDthread(&ptErrorLED));
  PT_SCHEDULE(boundaryControlThread(&ptBoundaryControl));
  PT_SCHEDULE(commsThread(&ptComms));

  





}
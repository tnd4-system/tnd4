#ifndef SMC3FUNC_H
#define SMC3FUNC_H

#include <Arduino.h>
#include <stdint.h>


//Block of stuff taken from SMC3. Its gonna stay here until I find a better solution.
// https://github.com/SimulatorMotorController/SMC3

// defines for setting and clearing register bits

#ifndef cbi
    #define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
    #define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif 



#endif // END GLOBALS 

extern void smcPWMWrite(uint8_t pin, uint8_t val);
extern void setPwmFrequency(int pin, int divisor);
extern void  InitialisePWMTimer1(unsigned int Freq);
extern void InitialisePWMTimer2(unsigned int Freq);

//END of SMC3 code Block.
